<!--
SPDX-FileCopyrightText: 2017 Matthieu Gallien <matthieu_gallien@yahoo.fr>

SPDX-License-Identifier: LGPL-3.0-or-later
-->

# Elisa

Музыкальный плеер Elisa

## Вступление

Elisa - это простой музыкальный проигрыватель, цель которого - обеспечить приятные впечатления своим пользователям.
Elisa позволяет просматривать музыку по альбомам, исполнителям или всем трекам. Музыка проиндексирована
с помощью частного индексатора или индексатора с помощью Baloo. Частный индексатор может быть
настроен для сканирования музыки по выбранным путям. Baloo намного быстрее, потому что
Baloo предоставляет все необходимые данные из собственной базы данных. Вы можете создать и проигрывать свой любимый плейлист

![Screenshot Elisa albums view](https://community.kde.org/images.community/3/35/Elisa_albums_view.png)

![Screenshot Elisa "Now Playing"" view](https://community.kde.org/images.community/7/75/Elisa_now_playing_view.png)


## Вклады в развитие

Взносы очень приветствуются. Элиза следует дизайну команды KDE VDG
(https://community.kde.org/KDE_Visual_Design_Group/Music_Player).
Пожалуйста, используйте экземпляр Gitlab из KDE (https://invent.kde.org/multimedia/elisa/) чтобы внести свой вклад.

